#!/bin/bash
lint_user_read_service () {
	echo "  Running linting for User read service" && (cd Domains/User/ReadService; yarn run lint)
}

lint_user_write_service () {
	echo "  Running linting for User write service" && (cd Domains/User/WriteService; yarn run lint)
}

if [ -n "$1" ]; then
	case "$1" in
		all) echo "Linting all services..." && lint_user_read_service && lint_user_write_service;;
		user)
			if [ -n "$2" ]; then
				case "$2" in
					-r) lint_user_read_service;;
					--read) lint_user_read_service;;
					-w) lint_user_write_service;;
					--write) lint_user_write_service;;
					*) echo "'$2' is not a proper option for service type. Try -r or --read options for read service and -w or -write options for write service";;
				esac
			else
				echo "Linting all User services..."
				lint_user_read_service
				lint_user_write_service
			fi;;
		*) echo "No services with '$1' name found";;
		esac
else
  echo "Please provide proper linting service name parameter: 'user' with '-r/-read/-w/-write' service type option or 'all'"
fi
