# FunBox

FunBox is a Discord Bot application which is going to be the best and diverse box of functionalities full of fun.

# Installation

To work with FunBox, you need to have proper environment prepared:

- [Node.js](https://nodejs.org/en/)
- [npm](https://www.npmjs.com)
- [yarn](https://classic.yarnpkg.com/en/docs/install)

You can verify installation of each by bash command:

```bash
node -v
npm -v
yarn -v
```

# Setup

When every component is installed you need a proper setup of working environment.

```bash
yarn
```

Command above will install or update all packages currently being used by application. Next, create `.env` file with data:

```bash
DISCORD_BOT_TOKEN=your_discord_bot_access_token
```

`DISCORD_BOT_TOKEN` variable holds token required by the application to connect to [Discord](https://discord.com) bot created for this purpose.

# Working with application

After setting everything up, you simply need to launch the project. To start the environment you need to use script described in `package.json`.

```bash
yarn run dev
```

You may need to wait some time, depending on hardware and operating system that is being used. First, you will notice `[nodemon]` logs and then `Ready!` message, indicating that everything started up as intended. From now on, bot added to any Discord servers will work using local environment. Nodemon will also react immediately to any codebase changes, so it will reload the application fully.

# Contributing

Any contribution is welcome. To join team or help us develop as a freelancer, follow [CONTRIBUTING](CONTRIBUTING.md) guide.

# License

TBD
