import { Express, Request, Response } from 'express';
import {
  Query,
  QueryDispatcher,
  IQueryDispatcher,
  QueryData,
  NewableDependency,
  QueryExecutor,
} from 'cqrs-di';
import { CheckUserBalanceQuery } from '../Queries/UserQueries';
import executors from '../Queries/QueryExecutors';

export class Service {
  private queryDispatcher: IQueryDispatcher<
    Query,
    QueryData,
    NewableDependency<QueryExecutor<Query, QueryData>>
  >;

  constructor() {
    this.queryDispatcher = new QueryDispatcher(executors);
  }

  public createRoutes(App: Express): void {
    App.get('/balance/:userId', this.getBalance.bind(this));
  }

  private async getBalance(req: Request, res: Response): Promise<void> {
    // Validate params
    const { userId } = req.params;
    const checkUserBalanceQuery = new CheckUserBalanceQuery(userId);
    await this.queryDispatcher
      .dispatch(checkUserBalanceQuery)
      // TODO: Data should of type CheckUserBalanceData - find a way for simple cast
      .then((data: any) => {
        if (data) {
          res.status(200).send({ data });
        } else {
          res.status(400).send({ error: 'Error: No data returned' });
        }
      })
      .catch((error: Error) => {
        console.error('Handled error: ', error);
        res.status(400).send({ error: error });
      });
  }
}
