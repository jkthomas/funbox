import { Entity } from 'cqrs-di';

export class User extends Entity {
  private _balance: number;

  constructor(id: string, balance: number) {
    super(id);
    this._balance = balance;
  }

  public get balance(): number {
    return this._balance;
  }

  work(): void {
    const amount = Math.floor(Math.random() * 20 + 1);
    this._balance += amount;
  }

  worked(): void {
    console.log('User worked placeholder. New balance: ', this._balance);
  }
}
