import { QueryData } from 'cqrs-di';

// INFO: Put all user Queries data below (in this same file)

export interface CheckUserBalanceData extends QueryData {
  balance: number;
}
