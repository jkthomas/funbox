import { CheckUserBalanceQueryExecutor } from './CheckUserBalanceQueryExecutor';

export default [CheckUserBalanceQueryExecutor];

// Here we add each new QueryExecutor for Resolver use
