// import { QueryExecutor, FileStorage } from 'cqrs-di';
import { QueryExecutor } from 'cqrs-di';
import { User } from '../../Entity/User';
// import UserRepository from '../../Repository/UserRepository';
import { CheckUserBalanceQuery } from '../UserQueries';
import { CheckUserBalanceData } from '../UserQueriesData';

export class CheckUserBalanceQueryExecutor
implements QueryExecutor<CheckUserBalanceQuery, CheckUserBalanceData> {
  async execute(query: CheckUserBalanceQuery): Promise<CheckUserBalanceData> {
    try {
      // TODO:
      // Add query validation
      // Add repository injection to Executors/Handlers
      // Change to actual DB operations
      // const userRepository = new UserRepository(new FileStorage('data'));
      // const user: any = await userRepository.findOne(query.userId);
      const user = new User('id', 100);
      if (!user) {
        throw `User with ID ${query.userId} not found`;
      }
      const data: CheckUserBalanceData = {
        balance: user.balance,
      };
      return data;
    } catch (error) {
      // TODO: Log and/or handle it - handle this domain error to UI friendly error for domain isolation
      console.error(error.message);
      throw error;
    }
  }
}
