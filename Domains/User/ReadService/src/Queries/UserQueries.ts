import { Query } from 'cqrs-di';

// INFO: Put all user Queries below (in this same file)

export class CheckUserBalanceQuery extends Query {
  userId: string;

  constructor(userId: string) {
    super('CheckUserBalanceQuery');
    this.userId = userId;
  }
}
