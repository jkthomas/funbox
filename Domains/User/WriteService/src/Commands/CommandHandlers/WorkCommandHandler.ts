import { User } from '../../Entity/User';
// import { NewableDependency, CommandHandler, FileStorage } from 'cqrs-di';
import { NewableDependency, CommandHandler } from 'cqrs-di';
// import UserRepository from '../../Repository/UserRepository';
import { WorkCommand } from '../UserCommands';

class WorkCommandHandler implements CommandHandler<WorkCommand> {
  async handle(command: WorkCommand): Promise<void> {
    try {
      // TODO:
      // Add command validation
      // Add repository injection to Executors/Handlers
      // Change to actual DB operations
      // The `new` instantiation will be removed upon fixing repository issues
      // const userRepository = new UserRepository(new FileStorage('data'));
      // const user: any = await userRepository.findOne(command.userId);
      const user = new User('id', 150);
      if (!user) {
        throw `User with ID ${command.userId} not found. Register new user with !register command.`;
      }
      const domainUser: User = new User(user.id, user.balance);
      domainUser.work();
      // await userRepository.update(domainUser);
      domainUser.worked();
    } catch (error) {
      // TODO: Log and/or handle it - handle this domain error to UI friendly error for domain isolation
      console.error(error.message);
      throw error;
    }
  }
}

const _WorkCommandHandler: NewableDependency<WorkCommandHandler> =
  WorkCommandHandler;

export default _WorkCommandHandler;
