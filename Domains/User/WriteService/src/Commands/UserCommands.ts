import { Command } from 'cqrs-di';

// INFO: Put all user Commands below (in this same file)

export class WorkCommand extends Command {
  userId: string;

  constructor(userId: string) {
    super('WorkCommand');
    this.userId = userId;
  }
}
