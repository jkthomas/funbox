import express from 'express';
import passport from 'passport';
import flash from 'express-flash';
import bodyParser from 'body-parser';
import lusca from 'lusca';
import morgan from 'morgan';
import dotenv from 'dotenv';
import { Service } from './Express/Service';
import { App } from './Express/App';
import cors from './Express/Middleware/CrossOriginResourceSharing';

dotenv.config();

const app = new App({
  port: parseInt(process.env.PORT || '4006'),
  middlewares: [
    passport.initialize(),
    passport.session(),
    flash(),
    bodyParser.json(),
    bodyParser.urlencoded({ extended: true }),
    lusca.xframe('SAMEORIGIN'),
    lusca.xssProtection(true),
    cors,
    morgan('combined'),
    express.json(),
    express.urlencoded({ extended: false }),
  ],
  services: [new Service()],
});

app.listen();
