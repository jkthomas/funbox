import express, { Express } from 'express';
import { Service } from './Service';

export class App {
  public app: Express;

  constructor(appParams: {
    port: number;
    middlewares: any;
    services: Service[];
  }) {
    this.app = express();

    this.initializeApp(appParams.port);
    this.initializeMiddlewares(appParams.middlewares);
    this.initializeServices(appParams.services);
  }

  private initializeApp(port: number) {
    this.app.set('port', port);
  }

  private initializeMiddlewares(middlewares: any) {
    middlewares.forEach((middleware: any) => {
      this.app.use(middleware);
    });
  }

  private initializeServices(services: Service[]) {
    services.forEach((service: Service) => {
      service.createRoutes(this.app);
    });
  }

  public listen(): void {
    this.app.listen(this.app.get('port'), () => {
      console.log(`App listening on port: ${this.app.get('port')}`);
    });
  }
}
