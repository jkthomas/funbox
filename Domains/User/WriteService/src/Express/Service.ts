import { Express, Request, Response } from 'express';
import {
  NewableDependency,
  CommandHandler,
  Command,
  ICommandDispatcher,
  CommandDispatcher,
} from 'cqrs-di';
import { WorkCommand } from '../Commands/UserCommands';
import handlers from '../Commands/CommandHandlers';

export class Service {
  private commandDispatcher: ICommandDispatcher<
    Command,
    NewableDependency<CommandHandler<Command>>
  >;

  constructor() {
    this.commandDispatcher = new CommandDispatcher(handlers);
  }

  public createRoutes(App: Express): void {
    App.put('/work/:userId', this.work.bind(this));
  }

  private async work(req: Request, res: Response): Promise<void> {
    // Validate body
    const { userId } = req.params;
    const workCommand = new WorkCommand(userId);
    await this.commandDispatcher
      .dispatch(workCommand)
      .then(() => {
        res.sendStatus(200);
      })
      .catch((error: Error) => {
        console.error('Handled error: ', error);
        res.status(400).send({ error });
      });
  }
}
