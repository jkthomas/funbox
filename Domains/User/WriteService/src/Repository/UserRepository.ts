import { Repository } from 'cqrs-di';

import { User } from '../Entity/User';

class UserRepository extends Repository<User> {}

export default UserRepository;
