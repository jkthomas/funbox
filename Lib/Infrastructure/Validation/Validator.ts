import { IValidator } from '../../Interface/Validation/IValidator';
import { ValidationResult } from '../../Interface/Validation/ValidationResult';
import { ValidationRuleOptions } from '../../Interface/Validation/ValidationRuleOptions';
import { ValidationRuleResult } from '../../Interface/Validation/ValidationRuleResult';
import { ValidationRule } from '../../Types/ValidationRule';

export class Validator implements IValidator {
  private rules: ValidationRule[] = [];

  public validate(): ValidationResult {
    const results: ValidationRuleResult[] = this.rules.map(
      (rule: ValidationRule) => rule(),
    );

    const isValid: boolean = results.every(
      (result: ValidationRuleResult) => result.isValid,
    );

    const messages: Array<string | null> | null = isValid
      ? null
      : results
          .map((result: ValidationRuleResult) => result.message)
          .filter(Boolean);

    return {
      isValid,
      messages,
    };
  }

  public createRule(options: ValidationRuleOptions) {
    return (): ValidationRuleResult => {
      const isValid: boolean = options.predicate();

      return {
        isValid,
        message: isValid ? null : options.message,
      };
    };
  }

  public addRules(rules: ValidationRule[]): void {
    if (!rules || rules.length === 0) {
      console.log('You need to specify at least one rule');
    }

    this.rules = [...rules, ...this.rules];
  }
}
