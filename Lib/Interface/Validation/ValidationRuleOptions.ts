export interface ValidationRuleOptions {
  predicate: (...data: any) => boolean;
  message: string;
}
