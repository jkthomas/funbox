export interface ValidationResult {
  isValid: boolean;
  messages: Array<string | null> | null;
}
