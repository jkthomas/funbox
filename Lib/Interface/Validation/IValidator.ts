import { ValidationRule } from '../../Types/ValidationRule';
import { ValidationResult } from './ValidationResult';
import { ValidationRuleOptions } from './ValidationRuleOptions';

export interface IValidator {
  validate: () => ValidationResult;
  createRule: (options: ValidationRuleOptions) => ValidationRule;
  addRules: (rules: ValidationRule[]) => void;
}
