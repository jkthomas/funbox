export interface ValidationRuleResult {
  isValid: boolean;
  message: string | null;
}
