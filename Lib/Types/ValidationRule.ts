import { ValidationRuleResult } from '../Interface/Validation/ValidationRuleResult';

export type ValidationRule = () => ValidationRuleResult;
