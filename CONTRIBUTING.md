# Issues

## Bug reports

When submitting a bug report, you should give full bug description and reproduction steps. It will help to fix or address proper code review and checks.

Bug reports must cover an actual inoperative or not working functionality from either UI or developer perspective. It may also be missing or unclear documentation on certain application aspects.

Before you report or start fixing a bug:

1. Make sure that an issue haven't been reported/fixed already.
2. Reproduce it with `develop` branch codebase.
3. Report the problem if providing a solution at once.

Please try to be as detailed as possible in your report. It will help to determine if it's an actual issue or expected behavior.

Bug reports are very important to us!

## Feature requests

We currently do not accept feature requests. There are too much features on our roadmap already. It will surely change in the future.

# Developers Team

We highly recommend joining our development team. We are using Kanban and conduct meetings. We also have Discord devs server on which we discuss problems, ask questions, notify about code reviews and test our solutions.

If you still want to contribute as a freelancer, make sure you still follow our rules. Thank you for the effort!

# Contribute to FunBox

Guide below provides information about contribution process.

## License

TBD

## Branch naming

To allow others to understand branch purpose at first sight, we introduced branches naming convention:

- bug/name-of-branch &mdash; is meant to fix a bug encountered in application
- feature/name-of-branch &mdash; is meant to add new application functionality without breaking its codebase
- extension/name-of-branch &mdash; is meant to extend current codebase (with either new feature or functionality wrapping) and will surely change current application's flow or behavior

## Pull requests

Title need to describe what changes were made when developing the branch. It should cover more information about branch purpose than branch name itself, but should not be too long.

Any PR should be first discussed with a developers team, as you could replicate an issue or feature that is already being developed.

Make sure to focus on your PR scope and don't add unrelated commits to it.

**DISLAIMER**: By submitting any of your code, you agree that it will be
licensed under the license used by the project.

Please follow linting and formatting configuration kept in the project. Each functionality should be covered by automated tests and then tested manually. Follow steps below to make sure your PR is as it should be:

1. [Fork](https://docs.gitlab.com/ee/gitlab-basics/fork-project.html) the project and configure the remotes.

2. Before you do any work on codebase, make sure you have its latest version:

   ```bash
   git checkout develop
   git pull upstream develop
   git push
   ```

3. Create a branch following branch naming convention (explained above).

4. Commit your changes in logical pieces of code. Keep your commit messages organized. Every commit should be properly described.

5. Make sure all the tests are still passing.

6. Push your branch to your fork.

7. [Create a Merge Request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html) following title convention (explained above). Make sure the target branch is `develop`.

8. Developers team will review your code and leave comments if anything will need additional work to be done. At least 2 approvals is needed for now, which may be increased as developers team grows. If everything is perfect, we will merge the changes and notify you about it.

Thank you for your contribution!
