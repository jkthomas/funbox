import dotenv from 'dotenv';
import { DiscordSocket } from './DiscordSocket';

dotenv.config();

const socket: DiscordSocket = new DiscordSocket();

socket.setUpDiscordSocket();
