import Discord from 'discord.js';
import { prefix } from './bot_config.json';

export class DiscordSocket {
  private _client: Discord.Client = new Discord.Client();

  setUpDiscordSocket(): void {
    console.log('[DISCORD] - Initiating bot client login...');
    this._client.login(process.env.DISCORD_BOT_TOKEN);
    // console.log(
    //   '[DISCORD] - Bot client logged in with options:',
    //   this._client.options,
    // );

    this._client.once('ready', () => {
      this.setUpMessages();
      console.log(
        '[DISCORD] - Discord client started listening for a messages! You can start using it now!',
      );
    });
  }

  private setUpMessages(): void {
    console.log('[DISCORD] - Initiating bot client messages handling...');
    this._client.on('message', async (message: Discord.Message) => {
      /**
       *  Example of Validator usage
       * */
      console.log('[DISCORD] - Message received', message.content);

      // TODO: Bot is replying multiple times (6) and sends multiple messages each ~5 seconds after first message is sent to it
      message.reply(`Template message: current prefix is set to ${prefix}!`);

      // const validator = new Validator();

      // const messageValidators = {
      //   isNotEqualToYoda: (messageContent: string) =>
      //     validator.createRule({
      //       message: 'Message can not be equal to Yoda',
      //       predicate: () => messageContent !== 'Yoda',
      //     }),
      //   messageHasContentField: (discordMessage: Discord.Message) =>
      //     validator.createRule({
      //       message: 'Message can not be equal to Yoda',
      //       predicate: () => !!discordMessage.content,
      //     }),
      //   isEqualToFrodo: (messageContent: string) =>
      //     validator.createRule({
      //       message: 'Message must be equal to Frodo',
      //       predicate: () => messageContent === 'Frodo',
      //     }),
      // };

      // validator.addRules([
      //   messageValidators.isNotEqualToYoda(message.content),
      //   messageValidators.messageHasContentField(message),
      //   messageValidators.isEqualToFrodo(message.content),
      // ]);

      // TODO: Uncomment in future use
      // const result = validator.validate();
      // console.log('VALIDATION RESULT : ', result);

      // if (!message.content.startsWith(prefix) || message.author.bot) return;
      // const args = message.content.slice(prefix.length).split(/ +/);
      // const command = args.shift()?.toLowerCase().substring(0);
      // if (!command) {
      //   message.reply('Command was not specified!');
      //   return;
      // }
      // console.log('[APP] - Looking for service');
      // let matchingServices: Array<Service> = [];
      // const exactService = services.find(
      //   (service: Service) => service.name.toLowerCase() === command,
      // );
      // if (exactService) {
      //   matchingServices.push(exactService);
      // } else {
      //   matchingServices = services.filter((service: Service) =>
      //     service.name.toLowerCase().includes(command),
      //   );
      // }
      // if (matchingServices.length < 1) {
      //   message.reply(`There is no such a service as: ${command}`);
      //   return;
      // }
      // if (matchingServices.length > 1) {
      //   message.reply(
      //     `There are more than 1 matches for a given service, please be more specific. Matched services: ${matchingServices}`,
      //   );
      //   return;
      // }
      // console.log('[APP] - Service found');

      // const readService: ReadService = new matchingServices[0].readService();
      // const writeService: WriteService = new matchingServices[0].writeService();
      // const params = message.content.split(' ');
      // params.shift();

      // let isNoWriteAction: boolean = false;
      // console.log('[APP] - Launching service method');
      // await writeService
      //   .delegateAction([...params, message.author.id])
      //   .then(() => {
      //     message.reply(
      //       // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      //       // @ts-ignore
      //       'Write service action placeholder reply. This message will be managed by eventBus callback',
      //     );
      //   })
      //   .catch((error: Error) => {
      //     // TODO: During adding error typing -> if the error was MISSING/MULTIPLE ACTION -> do not reply but check other service (readService) else handle error
      //     if (error.message.includes('There is no such an action as:')) {
      //       isNoWriteAction = true;
      //     } else {
      //       message.reply(`error occured: ${error}`);
      //     }
      //   });

      // if (isNoWriteAction) {
      //   await readService
      //     .delegateAction([...params, message.author.id])
      //     .then((result: Result) => {
      //       if (result.isSuccessful) {
      //         // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      //         // @ts-ignore
      //         message.reply(`your balance is $${result.data.balance}.`);
      //       } else {
      //         message.reply(`something went wrong: ${result.errorMessage}`);
      //       }
      //     })
      //     .catch((error: Error) => {
      //       message.reply(`error occured: ${error}`);
      //     });
      // }
    });
    console.log('[DISCORD] - Bot client messages handling ready!');
  }
}
